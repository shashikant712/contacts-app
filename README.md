# Contacts-App

Contacts App is developed in JavaScript and NodeJS and works in terminal. User provides specified input in terminal and get outputs in terminal too. Contacts App is used to Create a Contact, Read a Contact, Remove a Contact and Read all Contacts. A Contact body contains `name` , `mobile` and `email`.

### Demo : [CodeSandBox](https://codesandbox.io/s/contacts-terminal-i3ny4)

###### ( Add a new writable terminal in codesandbox and run the commnads )

## Commands with Demo Images:

###### ( Run commands in terminal )

### Create a New Contact

`node app.js add --name="Shashi Kant Yadav" --phone="9876543210" --email="shashikant@example.com"`

![Demo Image](/Images/demo1.png)

If Mobile number already present, It will show error

![Demo Image](/Images/demo6.png)

### Remove a Contact

`node app.js remove --name="Shashi Kant Yadav"`

![Demo Image](/Images/demo2.png)

If Contact is not present in JSON file, then it will show error

![Demo Image](/Images/demo5.png)

### Read a Contact

`node app.js read --name="Shashi Kant Yadav"`

![Demo Image](/Images/demo3.png)

If Contact not found then it will show an error

![Demo Image](/Images/demo7.png)

### List All Contact

`node app.js list`

![Demo Image](/Images/demo4.png)

If No contacts in JSON File, then it will show something like this

![Demo Image](/Images/demo8.png)

## Built With :

- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [NodeJS](https://nodejs.org/en/)
- [Chalk](https://www.npmjs.com/package/chalk)
- [Yargs](https://www.npmjs.com/package/yargs)

## Developed By :

- [Shashi Kant Yadav](https://bitbucket.org/%7B5374df3f-07c6-4a04-aa51-3e2f5760fa6a%7D)
